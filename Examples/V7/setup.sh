# Setup a minimal environment to run the examples
. /cvmfs/sft.cern.ch/lcg/releases/LCG_95rc1/ROOT/6.16.00/x86_64-centos7-gcc62-opt/ROOT-env.sh
export GARFIELD_HOME=/afs/cern.ch/user/d/djanssen/Tools/Garfield
export HEED_DATABASE=$GARFIELD_HOME/Heed/heed++/database
export LD_LIBRARY_PATH=$GARFIELD_HOME/build/lib:$LD_LIBRARY_PATH
